window.onload = () => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) {
    location.replace("/login");
  } else {
    const playerList = document.querySelector("#players-list");
    const timePlace = document.querySelector("#time");
    const textField = document.querySelector("#text");
    const message = document.querySelector("#message");
    const comments = document.querySelector("#comments");

    const inputField = document.querySelector("#input-form");
    const joinGame = document.querySelector("#join-game");

    const socket = io.connect({ query: { token: jwt } });

    const setTime = seconds => {
      let time = seconds;
      timePlace.innerHTML = time;

      function timeout() {
        setTimeout(() => {
          time--;
          timePlace.innerHTML = time;
          if (time) {
            timeout();
          }
        }, 1000);
      }
      timeout();
    };
    // ui functions
    const displayNewPlayer = login => {
      const newItem = document.createElement("li");
      const span = document.createElement("span");
      newItem.classList.add("list-group-item");
      span.id = `score-${login}`;
      newItem.innerHTML = `Гравець ${login}`;
      newItem.appendChild(span);
      newItem.innerHTML += `<div class="progress">
      <div id="score-${login}-pg" class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax=""></div>
    </div>`;
      playerList.appendChild(newItem);
    };

    const onPlayerLeave = login => {
      const item = document.querySelector(`#score-${login}-pg`);
      item.classList.add("bg-danger");
      item.style.width = "100%";
    };

    const updateScore = ({ login, score, percentage }) => {
      const item = document.querySelector(`#score-${login}-pg`);
      item.style.width = `${score * percentage}%`;
      item.setAttribute("aria-valuenow", score);
    };

    const displayUsers = arrOfUsers => {
      playerList.innerHTML = "";
      arrOfUsers.forEach(li => {
        playerList.append(li);
      });
    };

    const endGame = () => {
      message.innerHTML = `Незабаром почнется нова гра!`;
      inputField.disabled = true;
    };

    const displayComment = text => {
      comments.innerHTML = text;
    };

    const defaultSettings = () => {
      inputField.disabled = false;
      message.innerHTML = "";
      textField.innerHTML = "";
    };

    const displayText = fragment => index => {
      const span = document.createElement("span");
      span.id = `letter-${index}`;
      span.innerHTML = fragment + " ";
      textField.appendChild(span);
    };

    const displayError = () => {
      message.innerHTML = "Помилка!";
      message.classList.add("current");
      setTimeout(() => {
        message.innerHTML = "";
      }, 2000);
    };

    const addSelect = current => next => {
      current.classList.add("current");
      if (next) {
        next.classList.add("next");
      }
    };

    const removeSelect = current => next => {
      current.classList.remove("current");
      next.classList.remove("next");
    };
    // main logic
    joinGame.addEventListener("click", () => {
      joinGame.style.display = "none";
      socket.emit("joinToGame", { token: jwt });
    });

    socket.on("startTimer", ({ time, func }) => {
      setTime(time, func);
    });

    socket.on("comment", ({ text }) => {
      displayComment(text);
    });

    socket.on("newPlayer", ({ login }) => {
      displayNewPlayer(login);
    });

    socket.on("leavePlayer", ({ login }) => {
      onPlayerLeave(login);
    });

    socket.on("updateScore", ({ login, score, percentage }) => {
      updateScore({ login, score, percentage });
      const lis = document.querySelectorAll("li");
      const sortedLi = Array.from(lis).sort((first, second) => {
        return (
          second.querySelector(".progress-bar").getAttribute("aria-valuenow") -
          first.querySelector(".progress-bar").getAttribute("aria-valuenow")
        );
      });
      displayUsers(sortedLi);
    });

    socket.on("endGame", () => {
      endGame();
    });

    socket.on("play", ({ text }) => {
      defaultSettings();
      const fragments = text.split(" ");
      fragments.forEach((fragment, index) => {
        displayText(fragment)(index);
      });

      const percentage = 100 / fragments.length;

      let counter = 0;
      let current = document.querySelector(`#letter-${counter}`);
      let next = document.querySelector(`#letter-${counter + 1}`);
      addSelect(current)(next);
      inputField.addEventListener("keydown", evt => {
        if (evt.keyCode === 32) {
          if (inputField.value.replace(/\s/g, "") === fragments[counter]) {
            removeSelect(current)(next);
            if (fragments.length - 2 === counter) {
              counter++;
              current = document.querySelector(`#letter-${counter}`);
              addSelect(current)();
              socket.emit("nearFinish");
              socket.emit("correctAnswer", { token: jwt, percentage });
            } else if (fragments.length - 2 < counter) {
              counter++;
              socket.emit("correctAnswer", { token: jwt, percentage });
              socket.emit("playerEnd", { token: jwt });
            } else {
              counter++;
              current = document.querySelector(`#letter-${counter}`);
              next = document.querySelector(`#letter-${counter + 1}`);
              addSelect(current)(next);
              socket.emit("correctAnswer", { token: jwt, percentage });
            }
          } else {
            displayError();
          }
          inputField.value = "";
        }
      });
    });
  }
};
