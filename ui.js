export default class UI {
  constructor(playerList, timePlace, textField, message, inputField, joinGame) {
    this.playerList = playerList;
    this.timePlace = timePlace;
    this.textField = textField;
    this.message = message;
    this.innerHTML = inputField;
    this.joinGame = joinGame;
  }

  defaultSettings = () => {
    this.inputField.disabled = false;
    this.message.innerHTML = "";
    this.textField.innerHTML = "";
  };
  displayNewPlayer = login => {
    const newItem = document.createElement("li");
    const span = document.createElement("span");
    newItem.classList.add("list-group-item");
    span.id = `score-${login}`;
    newItem.innerHTML = `Гравець ${login}`;
    newItem.appendChild(span);
    newItem.innerHTML += `<div class="progress">
        <div id="score-${login}-pg" class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax=""></div>
      </div>`;
    this.playerList.appendChild(newItem);
  };

  updateScore = ({ login, score, percentage }) => {
    const item = document.querySelector(`#score-${login}-pg`);
    item.style.width = `${score * percentage}%`;
    item.setAttribute("aria-valuenow", score);
  };
  displayUsers = arrOfUsers => {
    this.playerList.innerHTML = "";
    arrOfUsers.forEach(li => {
      this.playerList.append(li);
    });
  };
  displayText = (fragment, index) => {
    const span = document.createElement("span");
    span.id = `letter-${index}`;
    span.innerHTML = fragment + " ";
    this.textField.appendChild(span);
  };
  addSelect = (current, next) => {
    current.classList.add("current");
    if (next) {
      next.classList.add("next");
    }
  };
  removeSelect = (current, next) => {
    current.classList.remove("current");
    next.classList.remove("next");
  };
  displayError = () => {
    this.message.innerHTML = "Помилка!";
    this.message.classList.add("current");
    setTimeout(() => {
      this.message.innerHTML = "";
    }, 2000);
  };
  onPlayerLeave = login => {
    const item = document.querySelector(`#score-${login}-pg`);
    item.classList.add("bg-danger");
    item.style.width = "100%";
  };
  playerEnd = login => {
    this.message.innerHTML = `Гравець ${login} закінчив гінку`;
  };
  endGame = () => {
    this.timePlace.innerHTML = "-";
    this.message.innerHTML = `Незабаром почнется нова гра!`;
    this.inputField.disabled = true;
  };
}
