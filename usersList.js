class JoinedUser {
  constructor(login, score, usedTime) {
    this.login = login;
    this.score = score;
    this.usedTime = usedTime;
  }
}

class FinishedUser {
  constructor(login) {
    this.login = login;
  }
}

class Use {
  constructor(login, score, usedTime) {
    this.login = login;
    this.score = score;
    this.usedTime = usedTime;
  }
  create(type) {
    let user;
    if (type === "joinedUsers") {
      user = new JoinedUser(this.login, this.score, this.usedTime);
    } else if (type === "finishedUsers") {
      user = new FinishedUser(this.login);
    }
        
  }
}

module.exports = User;
