const jokes = require("./jokes.json");

class CommentBot {
  greeting() {
    return "На вулиці зараз трохи пасмурно, але на Львів Арена зараз просто чудова атмосфера: двигуни гарчать, глядачі посміхаються а гонщики ледь помітно нервують та готуюуть своїх залізних конів до заїзду. А коментувати це все дійстово Вам буду я, Ескейп Ентерович і я радий вас вітати зі словами Доброго Вам дня панове!";
  }

  joke() {
    return jokes[Math.round(Math.random() * 3)];
  }

  getListPlayer(playerList) {
    return (
      `А тим часом список гонщиків: \n` +
      playerList.map((player, index) => {
        return ` ${player.login} на cвоїй клавіатурі під номером ${index +
          1}\n`;
      })
    );
  }
  getTime(playerList) {
    return (
      "Поточна ситуація така: \n" +
      playerList.map((player, index) => {
        return `На ${index + 1} місці гравець ${player.login} \n`;
      })
    );
  }
  onNearFinish(player) {
    return `${player} вже наближається до фінішної прямої!`;
  }
  onPlayerFinish(player) {
    return `Фінішну пряму пересікає ${player}`;
  }
  onGameFinish(playerList) {
    return (
      "І ось всі гравці завершили гінку, фінальний результат: \n" +
      playerList.map((player, index) => {
        return `${index + 1} місце посідає ${player.login} який набрав ${
          player.score
        } очків за ${(player.usedTime / 1000).toFixed(2)} секунд \n`;
      })
    );
  }
  parting() {
    return "Ну, а з вами був Ескейп Ентерович! Дякую за увагу і до зустрічі:)";
  }
}

module.exports = CommentBot;
