FROM node:10

COPY package*.json ./
COPY . .
COPY server.js ./

RUN npm install

ENV PORT 3000

CMD ["node", "server.js"]
EXPOSE 3000